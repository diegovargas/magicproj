'use strict';
const AppController = require('./AppController');

class ExamplesController extends AppController {

    init() {
        this.model('Example');
    }

    get(callback) {
        this.Example.findBy(this.query).
            then(res => {
                callback(res);
            }).
            catch(err => {
                callback(err);
            });
    }

    post(callback) {
        this.Example.save(this.payload).
            then(res => {
                callback(res);
            }).
            catch(err => {
                callback(err);
            });
    }
}
module.exports = ExamplesController;
