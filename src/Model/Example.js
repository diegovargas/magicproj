'use strict';
var AppModel = require('vmagic/AppModel');

class Example extends AppModel {
    init() {
        //Table name
        this.useTable = 'user';
        this.logger = this.component('Logger');
        this.validate([
            {"name" : {
                "rule" : "notEmpty",
                "message" : "field name cannot be empty."}},
            {"username" : {
                "rule" : "notEmpty",
                "message" : "field username cannot be empty."}},
            {"password" : {
                "rule" : "notEmpty",
                "message" : "field password cannot be empty."}}
        ]);
    }
}
module.exports = Example;
